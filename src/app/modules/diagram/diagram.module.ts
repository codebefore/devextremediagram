import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiagramLayoutComponent } from './diagram-layout/diagram-layout.component';
import { RouterModule } from '@angular/router';
import { DxDiagramModule } from 'devextreme-angular';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [DiagramLayoutComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path:'',
        component:DiagramLayoutComponent
      }
    ]),
    HttpClientModule,
    DxDiagramModule,
  ]
})
export class DiagramModule { }
