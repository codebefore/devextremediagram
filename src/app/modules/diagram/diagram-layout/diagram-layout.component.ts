import { Component, ViewChild } from '@angular/core';
import { DxDiagramComponent } from 'devextreme-angular';
import { DiagramService } from '../diagram.service';
@Component({
  selector: 'app-diagram-layout',
  templateUrl: './diagram-layout.component.html',
  styleUrls: ['./diagram-layout.component.scss'],
  providers:[DiagramService]
})
export class DiagramLayoutComponent  {
public employees:any[];
public instance:string='';
  @ViewChild(DxDiagramComponent, { static: false }) diagram: DxDiagramComponent;
  
    constructor(private service:DiagramService) {
   
     
    }
  ngOnInit() {
    this.employees=this.service.getEmployees();
    // this.diagram.instance.import(JSON.stringify(this.employees));
  }

  diagramExport():void{
   this.instance = this.diagram.instance.export();
   console.log(JSON.parse(this.instance));
    
  }

  diagramExport2():void{
    const instance2 = this.diagram.instance.export();
      console.log(JSON.parse(instance2));
    }

    diagramImport():void{
      console.log(JSON.parse(this.instance));
      this.diagram.instance.import(this.instance);
    }

}
